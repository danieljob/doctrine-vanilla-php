<?php

namespace Dan\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Proyecto
 *
 * @ORM\Table(name="proyecto", indexes={@ORM\Index(name="IDX_6FD202B9C929C89D", columns={"numerodptoproyecto"})})
 * @ORM\Entity
 */
class Proyecto
{
    /**
     * @var integer
     *
     * @ORM\Column(name="numproyecto", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="proyecto_numproyecto_seq", allocationSize=1, initialValue=1)
     */
    private $numproyecto;

    /**
     * @var string
     *
     * @ORM\Column(name="nombreproyecto", type="string", length=255, nullable=true)
     */
    private $nombreproyecto;

    /**
     * @var string
     *
     * @ORM\Column(name="ubicacionproyecto", type="string", length=100, nullable=true)
     */
    private $ubicacionproyecto;

    /**
     * @var \Dan\Entity\Departamento
     *
     * @ORM\ManyToOne(targetEntity="Dan\Entity\Departamento")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="numerodptoproyecto", referencedColumnName="numerodpto")
     * })
     */
    private $numerodptoproyecto;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Dan\Entity\Empleado", mappedBy="numproy")
     */
    private $codempleado;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->codempleado = new \Doctrine\Common\Collections\ArrayCollection();
    }

}
