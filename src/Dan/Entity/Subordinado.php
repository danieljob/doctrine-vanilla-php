<?php

namespace Dan\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Subordinado
 *
 * @ORM\Table(name="subordinado", indexes={@ORM\Index(name="IDX_55B9BAC83F29AEBB", columns={"codempleado"})})
 * @ORM\Entity
 */
class Subordinado
{
    /**
     * @var string
     *
     * @ORM\Column(name="nombresubordinado", type="string", length=100, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $nombresubordinado;

    /**
     * @var string
     *
     * @ORM\Column(name="sexo", type="string", length=1, nullable=true)
     */
    private $sexo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechanac", type="date", nullable=true)
     */
    private $fechanac;

    /**
     * @var string
     *
     * @ORM\Column(name="relacion", type="string", length=50, nullable=true)
     */
    private $relacion;

    /**
     * @var \Dan\Entity\Empleado
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Dan\Entity\Empleado")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="codempleado", referencedColumnName="cod")
     * })
     */
    private $codempleado;


}
