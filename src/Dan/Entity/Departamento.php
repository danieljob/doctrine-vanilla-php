<?php

namespace Dan\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Departamento
 *
 * @ORM\Table(name="departamento", indexes={@ORM\Index(name="IDX_40E497EBF860C219", columns={"coddirector"})})
 * @ORM\Entity
 */
class Departamento
{
    /**
     * @var integer
     *
     * @ORM\Column(name="numerodpto", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="departamento_numerodpto_seq", allocationSize=1, initialValue=1)
     */
    private $numerodpto;

    /**
     * @var string
     *
     * @ORM\Column(name="nombredpto", type="string", length=100, nullable=true)
     */
    private $nombredpto;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaingresodirector", type="date", nullable=true)
     */
    private $fechaingresodirector;

    /**
     * @var \Dan\Entity\Empleado
     *
     * @ORM\ManyToOne(targetEntity="Dan\Entity\Empleado")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="coddirector", referencedColumnName="cod")
     * })
     */
    private $coddirector;


}
