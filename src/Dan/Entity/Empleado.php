<?php

namespace Dan\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Empleado
 *
 * @ORM\Table(name="empleado", indexes={@ORM\Index(name="IDX_D9D9BF52F8105430", columns={"dep"}), @ORM\Index(name="IDX_D9D9BF5280408C2D", columns={"supercod"})})
 * @ORM\Entity
 */
class Empleado
{
    /**
     * @var integer
     *
     * @ORM\Column(name="cod", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="empleado_cod_seq", allocationSize=1, initialValue=1)
     */
    private $cod;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=50, nullable=true)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="apellido1", type="string", length=50, nullable=true)
     */
    private $apellido1;

    /**
     * @var string
     *
     * @ORM\Column(name="apellido2", type="string", length=50, nullable=true)
     */
    private $apellido2;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechanac", type="date", nullable=true)
     */
    private $fechanac;

    /**
     * @var string
     *
     * @ORM\Column(name="direccion", type="string", length=255, nullable=true)
     */
    private $direccion;

    /**
     * @var string
     *
     * @ORM\Column(name="sexo", type="string", length=1, nullable=true)
     */
    private $sexo;

    /**
     * @var float
     *
     * @ORM\Column(name="sueldo", type="float", precision=10, scale=0, nullable=true)
     */
    private $sueldo;

    /**
     * @var \Dan\Entity\Departamento
     *
     * @ORM\ManyToOne(targetEntity="Dan\Entity\Departamento")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="dep", referencedColumnName="numerodpto")
     * })
     */
    private $dep;

    /**
     * @var \Dan\Entity\Empleado
     *
     * @ORM\ManyToOne(targetEntity="Dan\Entity\Empleado")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="supercod", referencedColumnName="cod")
     * })
     */
    private $supercod;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Dan\Entity\Proyecto", inversedBy="codempleado")
     * @ORM\JoinTable(name="trabaja_en",
     *   joinColumns={
     *     @ORM\JoinColumn(name="codempleado", referencedColumnName="cod")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="numproy", referencedColumnName="numproyecto")
     *   }
     * )
     */
    private $numproy;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->numproy = new \Doctrine\Common\Collections\ArrayCollection();
    }

}
