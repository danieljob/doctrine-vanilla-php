<?php

namespace Dan\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LocalizacionesDpto
 *
 * @ORM\Table(name="localizaciones_dpto", indexes={@ORM\Index(name="IDX_DB5C364C2F93886E", columns={"numerodpto"})})
 * @ORM\Entity
 */
class LocalizacionesDpto
{
    /**
     * @var string
     *
     * @ORM\Column(name="ubicaciondpto", type="string", length=100, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $ubicaciondpto;

    /**
     * @var \Dan\Entity\Departamento
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Dan\Entity\Departamento")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="numerodpto", referencedColumnName="numerodpto")
     * })
     */
    private $numerodpto;


}
