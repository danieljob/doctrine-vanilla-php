<?php
// bootstrap.php
require_once "vendor/autoload.php";
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;



// Create a simple "default" Doctrine ORM configuration for Annotations
$isDevMode = true;
$config = Setup::createAnnotationMetadataConfiguration(
   // array(__DIR__."/src"), //path to map entities
   array(__DIR__."/src/Dan/Entity"), //path to map entities
    $isDevMode
    ,null
    ,null
    ,false

);
// or if you prefer XML
//$config = Setup::createXMLMetadataConfiguration(array(__DIR__."/config"), $isDevMode);
// database configuration parameters
$conn = array(
    'driver' => 'pdo_pgsql',
    'host'=>'localhost',
    'port'=>'5432',
    'user'=>'postgres',
    'password'=>'123456',
    'dbname'=>'empresa'
    //'charset'=>''    
    //'path' => __DIR__ . '/db.sqlite',
);

// obtaining the entity manager
$entityManager = EntityManager::create($conn, $config);